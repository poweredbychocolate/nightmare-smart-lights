package nightmares.smart.home.activities;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import nightmares.smart.home.R;
import nightmares.smart.home.adapters.SettingsAdapter;
import nightmares.smart.home.database.LightDAO;
import nightmares.smart.home.database.LightEntity;
import nightmares.smart.home.database.LocalDatabase;

/**
 * FragmentSettings add and remove switch connection.
 *
 * @author Dawid
 * @version 2
 * @since 2019-08-11
 */
public class FragmentSettings extends Fragment {
    private MainActivity mainActivity;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipe;
    private LightDAO dao;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        mainActivity = (MainActivity) getActivity();
        dao = LocalDatabase.get(mainActivity).lightDAO();
        swipe = view.findViewById(R.id.settings_swipe_refresh);
        recyclerView = view.findViewById(R.id.settings_list_view);

        FloatingActionButton fab = view.findViewById(R.id.settings_new);
        View addDialogView = inflater.inflate(R.layout.dialog_setting_new, null);

        EditText dialogName = addDialogView.findViewById(R.id.d_setting_name);
        EditText dialogAddress = addDialogView.findViewById(R.id.d_setting_address);
        EditText dialogSwitchId= addDialogView.findViewById(R.id.d_setting_light_id);

        AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
        builder.setView(addDialogView);
        builder.setCancelable(false).setPositiveButton(getString(R.string.dialog_add), (dialog, which) -> {
            LightEntity lightEntity = new LightEntity();
            lightEntity.setName(dialogName.getText().toString());
            lightEntity.setAddress(dialogAddress.getText().toString());
            lightEntity.setLightID(dialogSwitchId.getText().toString());
            dialogAddress.getText().clear();
            dialogName.getText().clear();
            dialogSwitchId.getText().clear();
            new Writer().execute(lightEntity);

        }).setNegativeButton(getString(R.string.dialog_cancel), (DialogInterface dialog, int id) -> dialog.cancel());

        AlertDialog addDialog = builder.create();
        fab.setOnClickListener(v -> addDialog.show());
        swipe.setOnRefreshListener(() -> {
            if (recyclerView.getAdapter() != null) {
                ((SettingsAdapter) recyclerView.getAdapter()).refreshAdapter();
            }
            swipe.setRefreshing(false);
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new SettingsAdapter(LocalDatabase.get(getContext()).lightDAO(), mainActivity));

    }

    /**
     * Writer, {@link AsyncTask} that save {@link LightEntity}.
     */
    class Writer extends AsyncTask<LightEntity, Void, Boolean> {

        @Override
        protected Boolean doInBackground(LightEntity... items) {

            try {
                for (LightEntity lightEntity : items) {
                    dao.save(lightEntity);
                }
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean status) {
            super.onPostExecute(status);
            if (status && recyclerView.getAdapter() != null) {
                ((SettingsAdapter) recyclerView.getAdapter()).refreshAdapter();
            }
        }
    }

}
