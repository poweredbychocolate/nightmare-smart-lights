package nightmares.smart.home.adapters;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import nightmares.smart.home.R;
import nightmares.smart.home.activities.MainActivity;
import nightmares.smart.home.database.LightDAO;
import nightmares.smart.home.database.LightEntity;

/**
 * Settings adapter management {@link LightEntity}.
 * @author Dawid
 * @version 1
 * @since 2019-08-11
 */
public class SettingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private LightDAO dao;
    private List<LightEntity> items;
    private MainActivity mainActivity;

    /**
     * Instantiates a new Settings adapter.
     *
     * @param lightDAO     the light dao
     * @param mainActivity the main activity
     */
    public SettingsAdapter(LightDAO lightDAO, MainActivity mainActivity) {
        this.dao = lightDAO;
        this.mainActivity = mainActivity;
        refreshAdapter();
    }

    /**
     * Refresh adapter, reload {@link LightEntity} list.
     */
    public void refreshAdapter() {
        new Reader().execute();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_server, viewGroup, false);
        return new SettingsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int index) {
        LightEntity light = items.get(index);
        SettingsViewHolder holder = ((SettingsViewHolder) viewHolder);
        holder.nameView.setText(light.getName());
        holder.addressView.setText(light.getAddress());
        View view = holder.view;
        view.setOnLongClickListener((v) -> {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mainActivity);
            dialogBuilder.setTitle(mainActivity.getString(R.string.dialog_def_title));
            dialogBuilder.setMessage(mainActivity.getString(R.string.dialog_def_msg));
            dialogBuilder.setPositiveButton(mainActivity.getString(R.string.dialog_yes), (dialog, which) -> {
                new Remover().execute(light);
            });
            dialogBuilder.setNegativeButton(mainActivity.getString(R.string.dialog_no), null);
            dialogBuilder.create().show();
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    private class SettingsViewHolder extends RecyclerView.ViewHolder {
        private TextView nameView;
        private TextView addressView;
        private RelativeLayout layout;
        private View view;

        /**
         * Instantiates a new Settings view holder.
         *
         * @param view the view
         */
        SettingsViewHolder(View view) {
            super(view);
            this.view = view;
            nameView = view.findViewById(R.id.server_name);
            addressView = view.findViewById(R.id.server_address);
            layout = view.findViewById(R.id.item_server);
        }

    }

    /**
     * Read {@link LightEntity} list using {@link AsyncTask}.
     */
    class Reader extends AsyncTask<Void, Void, List<LightEntity>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mainActivity.showProgressBar();
        }

        @Override
        protected List<LightEntity> doInBackground(Void... voids) {
            return dao.all();
        }

        @Override
        protected void onPostExecute(List<LightEntity> lightEntities) {
            super.onPostExecute(lightEntities);
            items = lightEntities;
            notifyDataSetChanged();
            mainActivity.hideProgressBar();
        }
    }

    /**
     * Remove {@link LightEntity} using {@link AsyncTask}.
     */
    class Remover extends AsyncTask<LightEntity, Void, Boolean> {

        @Override
        protected Boolean doInBackground(LightEntity... items) {

            try {
                for (LightEntity lightEntity : items) {
                    dao.remove(lightEntity);
                }
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean status) {
            super.onPostExecute(status);
            if (status) {
                refreshAdapter();
                mainActivity.reloadAdapters();
            }
        }
    }
}