package nightmares.smart.home.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.List;

/**
 * The type Group light entity.
 * @author Dawid
 * @version 1
 * @since 2019-08-28
 */
@Entity
public class GroupLightEntity implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    @NonNull
    private String name;
    @Ignore
    private List<LightEntity> list;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    @NonNull
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(@NonNull String name) {
        this.name = name;
    }

    /**
     * Gets list.
     *
     * @return the list
     */
    @NonNull
    public List<LightEntity> getList() {
        return list;
    }

    /**
     * Sets list.
     *
     * @param list the list
     */
    public void setList(List<LightEntity> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "GroupLightEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", list=" + list +
                '}';
    }
}
