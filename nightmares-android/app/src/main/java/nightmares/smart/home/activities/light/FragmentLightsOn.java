package nightmares.smart.home.activities.light;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import nightmares.smart.home.R;
import nightmares.smart.home.activities.MainActivity;
import nightmares.smart.home.adapters.light.LightSwitch;
import nightmares.smart.home.adapters.light.LightsOnAdapter;
import nightmares.smart.home.database.GroupLightEntity;
import nightmares.smart.home.database.LightDAO;
import nightmares.smart.home.database.LightEntity;
import nightmares.smart.home.database.LightJoinGroup;
import nightmares.smart.home.database.LocalDatabase;
import nightmares.smart.home.rest.CallResponse;
import nightmares.smart.home.rest.LightController;

/**
 * FragmentLightsOn display switches turn on progress.
 *
 * @author Dawid
 * @version 1
 * @since 2019-08-19
 */
public class FragmentLightsOn extends Fragment {
    private MainActivity mainActivity;
    private List<LightSwitch> switches;
    private RecyclerView recyclerView;
    private TextView counterText;
    private Button onAllButton;
    private LightDAO lightDAO;
    private AlertDialog onDialog;
    private View addDialogView;
    private AtomicInteger cCount;
    private AtomicInteger cAll;
    private LightSwitchsReader reader;
    private LightSwitcher switcher;
    private LightController lightController;
    private FloatingActionButton floatingNewGroup;
    private ListView listView;
    private GroupLightSelectAdapter groupLightSelectAdapter;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_light_auto, container, false);
        mainActivity = (MainActivity) getActivity();
        switches = new ArrayList<>();
        lightController = new LightController();
        lightDAO = LocalDatabase.get(mainActivity).lightDAO();
        recyclerView = view.findViewById(R.id.auto_list_view);
        counterText = view.findViewById(R.id.auto_counter);
        onAllButton = view.findViewById(R.id.auto_button);
        onAllButton.setText(getString(R.string.label_on_all));
        cCount = new AtomicInteger(0);
        cAll = new AtomicInteger(0);
        floatingNewGroup = view.findViewById(R.id.auto_new);
        floatingNewGroup.setEnabled(false);
        addDialogView = inflater.inflate(R.layout.dialog_group_new, null);
        start();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        recyclerView.setLayoutManager(new GridLayoutManager(mainActivity, 2));
        recyclerView.setAdapter(new LightsOnAdapter(mainActivity));
        // dialog for ask to perform off all action
        AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
        builder.setMessage(getString(R.string.dialog_auto_on));
        builder.setPositiveButton(getString(R.string.dialog_yes), (dialog, which) -> on());
        builder.setNegativeButton(getString(R.string.dialog_no), null);
        onDialog = builder.create();
        onAllButton.setOnClickListener(v -> onDialog.show());
        //New group dialog
        EditText groupName = addDialogView.findViewById(R.id.d_group_name);
        //Check list witch light switches
        listView = addDialogView.findViewById(R.id.d_group_list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setOnItemClickListener((parent, viewItem, position, id) -> {
            CheckedTextView checkedTextView = (CheckedTextView) viewItem;
            LightSwitch ls = groupLightSelectAdapter.getSwitches().get(position);
            if (checkedTextView.isChecked()) {
                groupLightSelectAdapter.getChecked().add(ls);
            } else {
                groupLightSelectAdapter.getChecked().remove(ls);
            }
        });
        builder = new AlertDialog.Builder(mainActivity);
        builder.setView(addDialogView);
        builder.setCancelable(false).setPositiveButton(getString(R.string.dialog_add), (dialog, which) -> {
            GroupLightEntity entity = new GroupLightEntity();
            entity.setName(groupName.getText().toString());
            groupName.getText().clear();
            entity.setList(new LinkedList<>());
            for (LightSwitch lightSwitch : groupLightSelectAdapter.getChecked()) {
                entity.getList().add(lightSwitch.getLightEntity());
            }
            new SaveGroup().execute(entity);
        });
        builder.setNegativeButton(getString(R.string.dialog_cancel), (DialogInterface dialog, int id) -> dialog.cancel());
        AlertDialog addDialog = builder.create();
        floatingNewGroup.setOnClickListener(v -> addDialog.show());
    }

    /**
     * Sets counter displayed above switches.
     *
     * @param count the turn on items count
     * @param all   the all items count
     */
    public void setCounter(Integer count, Integer all) {
        this.cCount.set(count);
        this.cAll.set(all);
        this.counterText.setText(count + " / " + all);
    }

    public Integer getCounterCount() {
        return cCount.get();
    }

    public Integer getCounterAll() {
        return cAll.get();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (recyclerView.getAdapter() != null) {
            if (hidden) {
                ((LightsOnAdapter) recyclerView.getAdapter()).stop();
                stop();
            } else {
                ((LightsOnAdapter) recyclerView.getAdapter()).start();
                start();
            }
        }
    }

    /**
     * Read {@link LightSwitch} list using {@link AsyncTask}.
     */
    class LightSwitchsReader extends AsyncTask<Void, LightSwitch, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mainActivity.showProgressBar();
            switches.clear();
            floatingNewGroup.setEnabled(false);
            setCounter(0, 0);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (LightEntity lightEntity : lightDAO.allByUsage()) {
                if (isCancelled()) break;
                publishProgress(new LightSwitch(lightEntity));
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(LightSwitch... values) {
            super.onProgressUpdate(values);
            switches.addAll(Arrays.asList(values));
            mainActivity.setOnCounter(0, getCounterAll() + 1);
        }

        @Override
        protected void onPostExecute(Void lsVoid) {
            super.onPostExecute(lsVoid);
            groupLightSelectAdapter = new GroupLightSelectAdapter(mainActivity, switches);
            listView.setAdapter(groupLightSelectAdapter);
            floatingNewGroup.setEnabled(true);
            mainActivity.hideProgressBar();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mainActivity.setOnCounter(0, 0);
            mainActivity.hideProgressBar();
        }
    }

    /**
     * The type Light switcher.
     */
    class LightSwitcher extends AsyncTask<LightSwitch, Integer, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mainActivity.showProgressBar();
        }

        @Override
        protected Boolean doInBackground(LightSwitch... lights) {
            try {
                int c = 0;
                for (LightSwitch light : lights) {
                    if (isCancelled()) break;
                    light.setResponse(lightController.on(light));
                    if (light.getResponse() == null) {
                        light.setState(LightSwitch.OFFLINE);
                    } else if (light.getResponse().getValue().equals(CallResponse.SUCCSESS)) {
                        light.setState(LightSwitch.ON);
                    }
                    publishProgress(++c, lights.length);
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mainActivity.setOnCounter(values[0], values[1]);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mainActivity.hideProgressBar();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mainActivity.hideProgressBar();
        }
    }

    /**
     * The type Save Group.
     */
    class SaveGroup extends AsyncTask<GroupLightEntity, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mainActivity.showProgressBar();
        }

        @Override
        protected Boolean doInBackground(GroupLightEntity... lights) {
            try {
                for (GroupLightEntity light : lights) {
                    long id = lightDAO.save(light);
                    light.setId((int) id);
                    for (LightEntity lightEntity : light.getList()) {
                        LightJoinGroup lightJoinGroup = new LightJoinGroup(light.getId(), lightEntity.getId());
                        lightDAO.save(lightJoinGroup);
                    }
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean && recyclerView.getAdapter() != null) {
                ((LightsOnAdapter) recyclerView.getAdapter()).start();
                groupLightSelectAdapter.clearChecked();
            }
            mainActivity.hideProgressBar();
        }
    }


    public void start() {
        if (reader != null && !reader.isCancelled()) reader.cancel(true);
        reader = new LightSwitchsReader();
        reader.execute();
    }

    public void on() {
        if (switcher != null && !switcher.isCancelled()) switcher.cancel(true);
        switcher = new LightSwitcher();
        switcher.execute(switches.toArray(new LightSwitch[switches.size()]));
    }

    public void stop() {
        if (switcher != null && !switcher.isCancelled()) {
            switcher.cancel(true);
        }
        if (reader != null && !reader.isCancelled()) {
            reader.cancel(true);
        }
    }
}
