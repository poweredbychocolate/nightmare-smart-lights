package nightmares.smart.home.activities.light;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import nightmares.smart.home.adapters.light.LightSwitch;

public class GroupLightSelectAdapter extends ArrayAdapter<LightSwitch> {
    private Context context;
    private List<LightSwitch> switches;
    private List<LightSwitch> checked;

    public GroupLightSelectAdapter(@NonNull Context context, @NonNull List<LightSwitch> switches) {
        super(context, 0, switches);
        this.context=context;
        this.switches=switches;
        this.checked = new LinkedList<>();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LightSwitch lightSwitch = switches.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(android.R.layout.simple_list_item_multiple_choice, null);
        TextView textView = view.findViewById(android.R.id.text1);
        textView.setText(lightSwitch.getName()+" ("+lightSwitch.getAddress()+" )");
        return view;
    }

    public List<LightSwitch> getSwitches() {
        return switches;
    }

    public List<LightSwitch> getChecked() {
        return checked;
    }
    public void  clearChecked(){checked.clear();}
}
