package nightmares.smart.home.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Execute get using given url, return JSON object as String
 * @version 1
 * @since 2019-08-11
 */
public interface RetrofitApi {
    @GET
    Call<String> get(@Url String url);
}
