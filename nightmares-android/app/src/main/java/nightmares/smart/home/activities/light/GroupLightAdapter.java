package nightmares.smart.home.activities.light;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import nightmares.smart.home.adapters.light.LightSwitch;
import nightmares.smart.home.database.LightEntity;

public class GroupLightAdapter extends ArrayAdapter<LightEntity> {
    private Context context;
    private List<LightEntity> list;

    public GroupLightAdapter(@NonNull Context context, @NonNull List<LightEntity> lightEntities) {
        super(context, 0, lightEntities);
        this.context=context;
        this.list=lightEntities;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LightEntity lightEntity = list.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(android.R.layout.simple_list_item_1, null);
        TextView textView = view.findViewById(android.R.id.text1);
        textView.setText(lightEntity.getName()+" ("+lightEntity.getAddress()+" )");
        return view;
    }

}
