package nightmares.smart.home.adapters.light;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import nightmares.smart.home.R;

/**
 * LightHolder single switch card.
 * @author Dawid
 * @since 2019-08-13
 * @version 1
 */
class LightHolder extends RecyclerView.ViewHolder {

    private CardView cardView;
    private TextView name;
    private View strip;
    private View view;

    /**
     * Instantiates a new Light holder.
     *
     * @param itemView the item view
     */
    public LightHolder(@NonNull View itemView) {
        super(itemView);
        view = itemView;
        cardView = itemView.findViewById(R.id.light_card);
        strip = itemView.findViewById(R.id.light_strip);
        name = itemView.findViewById(R.id.light_name);
    }

    /**
     * Gets card view.
     *
     * @return the card view
     */
    public CardView getCardView() {
        return cardView;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public TextView getName() {
        return name;
    }

    /**
     * Gets strip.
     *
     * @return the strip
     */
    public View getStrip() {
        return strip;
    }

    /**
     * Gets view.
     *
     * @return the view
     */
    public View getView() {
        return view;
    }
}