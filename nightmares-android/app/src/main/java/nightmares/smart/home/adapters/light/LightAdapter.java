package nightmares.smart.home.adapters.light;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nightmares.smart.home.R;
import nightmares.smart.home.activities.MainActivity;
import nightmares.smart.home.activities.light.FragmentSwitches;
import nightmares.smart.home.database.LightDAO;
import nightmares.smart.home.database.LightEntity;
import nightmares.smart.home.rest.CallResponse;
import nightmares.smart.home.rest.LightController;

/**
 * LightAdapter for {@link FragmentSwitches}.
 * @author Dawid
 * @version 2
 */
public class LightAdapter extends RecyclerView.Adapter<LightHolder> {
    //define count light state read on startup activity
    private final int FETCH=8;
    private LightDAO dao;
    private List<LightSwitch> lights;
    private MainActivity mainActivity;
    private LightController lightController;

    /**
     * Instantiates a new Switches adapter.
     *
     * @param lightDAO     the light dao
     * @param mainActivity the main activity
     */
    public LightAdapter(LightDAO lightDAO, MainActivity mainActivity) {
        this.lights= new ArrayList<>();
        this.dao = lightDAO;
        this.mainActivity = mainActivity;
        lightController = new LightController();
    }

    /**
     * Refresh adapter, reload {@link LightEntity} list.
     */
    public void refreshAdapter() {
        new LightSwitchesReader().execute();
    }

    @NonNull
    @Override
    public LightHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_light_card, viewGroup, false);
        return new LightHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LightHolder lightHolder, int i) {
        LightSwitch lightDef = lights.get(i);
        lightHolder.getName().setText(lightDef.getName());
        if (lightDef.hasState(LightSwitch.ON)) {
            lightHolder.getStrip().setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.lightOn));
        } else if (lightDef.hasState(LightSwitch.OFF)) {
            lightHolder.getStrip().setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.lightOff));
        } else if (lightDef.hasState(LightSwitch.OFFLINE)) {
            lightHolder.getStrip().setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.lightOffline));
        } else if (lightDef.hasState(LightSwitch.UNKNOWN)) {
            lightHolder.getStrip().setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.lightUnknown));
        }
        lightHolder.getView().setOnLongClickListener((v) -> {
            new LightSwitcher().execute(lightDef);
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return lights != null ? lights.size() : 0;
    }
    ////


    /**
     * Read {@link LightEntity} list using {@link AsyncTask}.
     */
    class LightSwitchesReader extends AsyncTask<Void, LightSwitch, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mainActivity.showProgressBar();
            lights.clear();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            int count=0;
            for (LightEntity lightEntity : dao.allByUsage()) {
                LightSwitch ls =new LightSwitch(lightEntity);
                //Get actual switch state
                if(count<FETCH) {
                    ls.setResponse(lightController.get(ls));
                    count++;
                }
                if (ls.getResponse() == null) {
                   ls.setState(LightSwitch.OFFLINE);
                } else if (ls.getResponse().getValue().equals(CallResponse.ON)) {
                    ls.setState(LightSwitch.ON);
                } else {
                    ls.setState(LightSwitch.OFF);
                }
                publishProgress(ls);
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(LightSwitch... values) {
            super.onProgressUpdate(values);
            lights.addAll(Arrays.asList(values));
            notifyDataSetChanged();
        }

        @Override
        protected void onPostExecute(Boolean sVoid) {
            super.onPostExecute(sVoid);
            mainActivity.hideProgressBar();
        }
    }

    /**
     * LightSwitcher call turn on and off .
     */
    class LightSwitcher extends AsyncTask<LightSwitch, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mainActivity.showProgressBar();
        }

        @Override
        protected Boolean doInBackground(LightSwitch... lights) {
            try {
                for (LightSwitch light : lights) {
                    if (light.hasState(LightSwitch.UNKNOWN)) {
                        light.setResponse(lightController.get(light));
                        if (light.getResponse() == null) {
                            light.setState(LightSwitch.OFFLINE);
                        } else if (light.getResponse().getValue().equals(CallResponse.ON)) {
                            light.setState(LightSwitch.ON);
                        } else {
                            light.setState(LightSwitch.OFF);
                        }
                    }
                    if (light.hasState(LightSwitch.ON)) {
                        light.setResponse(lightController.off(light));
                        if (light.getResponse() == null) {
                            light.setState(LightSwitch.OFFLINE);
                        } else if (light.getResponse().getValue().equals(CallResponse.SUCCSESS)) {
                            light.setState(LightSwitch.OFF);
                        }
                    } else {
                        light.setResponse(lightController.on(light));
                        if (light.getResponse() == null) {
                            light.setState(LightSwitch.OFFLINE);
                        } else if (light.getResponse().getValue().equals(CallResponse.SUCCSESS)) {
                            light.setState(LightSwitch.ON);
                            light.setUsageCount(light.getUsageCount() + 1);
                            dao.update(light);
                        }
                    }
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean status) {
            super.onPostExecute(status);
            mainActivity.hideProgressBar();
            notifyDataSetChanged();
        }
    }
}
