package nightmares.smart.home.rest;


/**
 * Light response contains device status.
 * @author Dawid
 * @version 2
 * @since 2019-08-11
 */
public class CallResponse {
    public static final Integer SUCCSESS  = 0;
    public static final Integer UNKNOWN_ID  = -100;
    public static final Integer ON  = 1;
    public static final Integer OFF  = 0;
    private Integer id;
    private Integer value;
    private String name;
    private String hardware;
    private Boolean connected;
    /**
     * Instantiates a new Light response.
     */
    public CallResponse() {
    }

    /**
     * Instantiates a new Light response.
     *
     * @param jsonResponse the json response
     */
    public CallResponse(String jsonResponse) {
        String[] parts =split(jsonResponse);
        this.value=Integer.parseInt(parts[1].trim());
        this.id=Integer.parseInt(parts[3].trim());
        this.name = parts[5].trim();
        this.hardware=parts[7].trim();
        this.connected = Boolean.parseBoolean(parts[9].trim());
    }

    /**
     * Split string response into parts.
     *
     * @param jsonResponse the json response
     * @return the string [ ]
     */
    private String[] split(String jsonResponse){
        jsonResponse = jsonResponse.replace("{", "");
        jsonResponse = jsonResponse.replace("}", "");
        jsonResponse = jsonResponse.replace("\"", "");
        return jsonResponse.split(":|,");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public Boolean getConnected() {
        return connected;
    }

    public void setConnected(Boolean connected) {
        this.connected = connected;
    }

    @Override
    public String toString() {
        return "CallResponse{" +
                "id=" + id +
                ", value=" + value +
                ", name='" + name + '\'' +
                ", hardware='" + hardware + '\'' +
                ", connected=" + connected +
                '}';
    }
}
