package nightmares.smart.home.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

@Database(entities = {LightEntity.class, GroupLightEntity.class,LightJoinGroup.class}, version = 7, exportSchema = false)
public abstract class LocalDatabase extends RoomDatabase {
    private static LocalDatabase localDatabase;

    //create statement from sqlite
    //CREATE TABLE "LightEntity" (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `address` TEXT NOT NULL, `lightID` TEXT NOT NULL, `usageCount` INTEGER NOT NULL, groupId INTEGER)
    public abstract LightDAO lightDAO();

    public static synchronized LocalDatabase get(Context context) {
        if (localDatabase == null) {
            localDatabase = Room.databaseBuilder(context.getApplicationContext(), LocalDatabase.class, "LightSwitchesDB").addMigrations(M1, M2, M3, M4,M5,M6).build();
        }
        return localDatabase;
    }

    static final Migration M1 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE LightDef RENAME TO LightEntity");
        }
    };
    static final Migration M2 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE GroupLightEntity(id INTEGR PRIMARY KEY,name TEXT NOT NULL)");
            database.execSQL("ALTER TABLE LightEntity ADD COLUMN groupId INTEGER");
        }
    };
    static final Migration M3 = new Migration(3, 4) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE GroupLightEntity RENAME TO tmp");
            database.execSQL("CREATE TABLE GroupLightEntity(id INT PRIMARY KEY,name TEXT NOT NULL)");
            database.execSQL("INSERT INTO GroupLightEntity(id, name) SELECT id, name FROM tmp");
            database.execSQL("DROP TABLE tmp;");
        }
    };
    static final Migration M4 = new Migration(4, 5) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE GroupLightEntity RENAME TO tmp");
            database.execSQL("CREATE TABLE \"GroupLightEntity\"(`id` INTEGER PRIMARY KEY AUTOINCREMENT,'name' TEXT NOT NULL)");
            database.execSQL("INSERT INTO GroupLightEntity(id, name) SELECT id, name FROM tmp");
            database.execSQL("DROP TABLE tmp;");
        }
    };
    static final Migration M5 = new Migration(5, 6) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
        }
    };
    static final Migration M6 = new Migration(6, 7) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE LightEntity RENAME TO tmpL");
            database.execSQL("CREATE TABLE \"LightEntity\" (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `address` TEXT NOT NULL, `lightID` TEXT NOT NULL, `usageCount` INTEGER NOT NULL)");
            database.execSQL("INSERT INTO LightEntity(id,name,address,lightId,usageCount) SELECT id,name,address,lightId,usageCount FROM tmpL");
            database.execSQL("DROP TABLE tmpL");
            database.execSQL("CREATE TABLE \"LightJoinGroup\"(`lightId` INTEGER  NOT NULL,`groupId` INTEGER  NOT NULL, PRIMARY KEY(groupID,lightId)," +
                             "FOREIGN KEY(lightId) REFERENCES LightEntity(id),FOREIGN KEY(groupId) REFERENCES GroupLightEntity(id))");
        }
    };
}
