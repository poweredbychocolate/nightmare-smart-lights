package nightmares.smart.home.adapters.light;

import android.support.annotation.NonNull;

import nightmares.smart.home.database.LightEntity;
import nightmares.smart.home.rest.CallResponse;

/**
 * LightSwitch extend {@link LightEntity}.
 *
 * @author Dawid
 * @version 2
 * @since 2019 -08-12
 */
public class LightSwitch extends LightEntity {
    /**
     * The constant OFF.
     */
    public static final Byte OFF = 0;
    /**
     * The constant ON.
     */
    public static final Byte ON = 1;
    /**
     * The constant OFFLINE.
     */
    public static final Byte OFFLINE = -10;
    /**
     * The constant UNKNOWN.
     */
    public static final Byte UNKNOWN = -100;

    private LightEntity lightEntity;
    private CallResponse response;
    private Byte state;

    /**
     * Instantiates a new Light switch.
     *
     * @param lightEntity the light def
     */
    public LightSwitch(LightEntity lightEntity) {
        this.lightEntity = lightEntity;
        this.state = UNKNOWN;
    }

    /**
     * Gets light def.
     *
     * @return the light def
     */
    public LightEntity getLightEntity() {
        return lightEntity;
    }

    /**
     * Sets light def.
     *
     * @param lightEntity the light def
     */
    public void setLightEntity(LightEntity lightEntity) {
        this.lightEntity = lightEntity;
    }


    /**
     * Gets response.
     *
     * @return the response
     */
    public CallResponse getResponse() {
        return response;
    }

    /**
     * Sets response.
     *
     * @param response the response
     */
    public void setResponse(CallResponse response) {
        this.response = response;
    }

    /**
     * Has state boolean.
     *
     * @param hasState the has state
     * @return true if has same state
     */
    public boolean hasState(Byte hasState) {
        return state.equals(hasState);
    }

    /**
     * Sets state.
     *
     * @param state the state
     */
    public void setState(Byte state) {
        this.state = state;
    }

    @Override
    public Integer getId() {
        return lightEntity.getId();
    }

    @Override
    public void setId(Integer id) {
        this.lightEntity.setId(id);
    }

    @NonNull
    @Override
    public String getName() {
        return lightEntity.getName();
    }

    @Override
    public void setName(@NonNull String name) {
        this.lightEntity.setName(name);
    }


    @NonNull
    @Override
    public String getAddress() {
        return lightEntity.getAddress();
    }

    @Override
    public void setAddress(@NonNull String address) {
        this.lightEntity.setAddress(address);
    }

    @NonNull
    @Override
    public Long getUsageCount() {
        return lightEntity.getUsageCount();
    }

    @Override
    public void setUsageCount(@NonNull Long usageCount) {
        this.lightEntity.setUsageCount(usageCount);
    }

    @Override
    public void setLightID(@NonNull String lightID) {
        this.lightEntity.setLightID(lightID);
    }

    @NonNull
    @Override
    public String getLightID() {
        return lightEntity.getLightID();
    }

    @Override
    public String toString() {
        return "LightSwitch{" +
                "lightEntity=" + lightEntity +
                ", response=" + response +
                ", state=" + state +
                "} " + super.toString();
    }
}
