package nightmares.smart.home.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * The type Light Entity.
 *
 * @author Dawid
 * @version 4
 * @since 2019-08-10
 */
@Entity
public class LightEntity implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    @NonNull
    private String name;
    @NonNull
    private String address;
    @NonNull
    private String lightID;
    @NonNull
    private Long usageCount;

    public LightEntity() {
        usageCount = 0L;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    @NonNull
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(@NonNull String name) {
        this.name = name;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    @NonNull
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(@NonNull String address) {
        this.address = address;
    }

    /**
     * Gets usage count.
     *
     * @return the usage count
     */
    @NonNull
    public Long getUsageCount() {
        return usageCount;
    }

    /**
     * Sets usage count.
     *
     * @param usageCount the usage count
     */
    public void setUsageCount(@NonNull Long usageCount) {

        if (usageCount > (Long.MAX_VALUE - 2)) this.usageCount = usageCount;
    }

    @NonNull
    public String getLightID() {
        return lightID;
    }

    public void setLightID(@NonNull String lightID) {
        this.lightID = lightID;
    }

    @Override
    public String toString() {
        return "LightEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", lightID='" + lightID + '\'' +
                ", usageCount=" + usageCount +
                '}';
    }
}
