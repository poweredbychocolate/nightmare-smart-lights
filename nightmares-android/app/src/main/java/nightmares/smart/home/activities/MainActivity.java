package nightmares.smart.home.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ProgressBar;
import java.util.concurrent.atomic.AtomicInteger;
import nightmares.smart.home.R;
import nightmares.smart.home.activities.light.FragmentLightsOff;
import nightmares.smart.home.activities.light.FragmentLightsOn;
import nightmares.smart.home.activities.light.FragmentSwitches;

/**
 * App MainActivity.
 *
 * @version 2
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private ProgressBar progressBar;
    private AtomicInteger progressCount;
    private String lastFragmentName;

    ////
    private FragmentSwitches fragmentSwitches;
    private FragmentSettings fragmentSettings;
    private FragmentLightsOn fragmentLightsOn;
    private FragmentLightsOff fragmentLightsOff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressCount = new AtomicInteger(0);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.light_menu);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.menu_switches);
        progressBar = findViewById(R.id.main_progress_bar);
        fragmentSwitches = new FragmentSwitches();
        fragmentSettings = new FragmentSettings();
        fragmentLightsOn = new FragmentLightsOn();
        fragmentLightsOff = new FragmentLightsOff();
        if (savedInstanceState != null && savedInstanceState.getString("LAST_FRAGMENT") != null) {
            showFragmentByName(savedInstanceState.getString("LAST_FRAGMENT"), navigationView);
        } else {
            showFragment(fragmentSwitches, navigationView);
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.menu_switches) {
            showFragment(fragmentSwitches, navigationView);
        } else if (id == R.id.menu_settings) {
            showFragment(fragmentSettings, navigationView);
        } else if (id == R.id.menu_auto_on) {
            showFragment(fragmentLightsOn,navigationView);
        } else if (id == R.id.menu_auto_off) {
            showFragment(fragmentLightsOff,navigationView);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("LAST_FRAGMENT", lastFragmentName);
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideFragments();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showFragmentByName(lastFragmentName, navigationView);
    }

    /**
     * Show top progress bar.
     */
////
    public void showProgressBar() {
        progressCount.incrementAndGet();
        this.progressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hide top progress bar.
     */
    public void hideProgressBar() {
        if (progressCount.decrementAndGet() <= 0) {
            this.progressBar.setVisibility(View.GONE);
            progressCount.set(0);
        }
    }

    private void showFragment(Fragment fragment, NavigationView navigationView) {
        hideFragments();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (fragment.isAdded())
            fragmentTransaction.show(fragment);
        else
            fragmentTransaction.add(R.id.main_fragment, fragment);
        lastFragmentName = fragment.getClass().getSimpleName();
        navigationView.setCheckedItem(fragment.getId());
        fragmentTransaction.commit();
    }

    private void hideFragments() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (fragmentSwitches.isAdded()) {
            fragmentSwitches.reload();
            fragmentTransaction.hide(fragmentSwitches);
        }
        if (fragmentSettings.isAdded()) {
            fragmentTransaction.hide(fragmentSettings);
        }
        if (fragmentLightsOn.isAdded()) {
            fragmentTransaction.hide(fragmentLightsOn);
        }
        if (fragmentLightsOff.isAdded()) {
            fragmentTransaction.hide(fragmentLightsOff);
        }
        fragmentTransaction.commit();
    }

    private void showFragmentByName(String fragmanName, NavigationView navigationView) {
        Fragment fragment = null;
        if (fragmentSwitches.getClass().getSimpleName().equals(fragmanName)) {
            fragment = fragmentSwitches;
        } else if (fragmentSettings.getClass().getSimpleName().equals(fragmanName)) {
            fragment = fragmentSettings;
        } else if (fragmentLightsOn.getClass().getSimpleName().equals(fragmanName)) {
            fragment = fragmentLightsOn;
        } else if (fragmentLightsOff.getClass().getSimpleName().equals(fragmanName)) {
            fragment = fragmentLightsOff;
        }
        if (fragment != null) {
            showFragment(fragment, navigationView);
        }
    }

    /**
     * Call adapters reload
     */
    public void reloadAdapters() {
        fragmentSwitches.reload();
    }

    /**
     * Call {@link FragmentLightsOn#setCounter(Integer, Integer)} set counter values
     * @param count the turn on items count
     * @param all   the all items count
     */
    public void setOnCounter(Integer count, Integer all) {
        fragmentLightsOn.setCounter(count, all);
    }

    /**
     * Call {@link FragmentLightsOff#setCounter(Integer, Integer)} set counter values
     * @param count the turn off items count
     * @param all   the all items count
     */
    public void setOffCounter(Integer count, Integer all) {
        fragmentLightsOff.setCounter(count, all);
    }
    public Integer getOffCounterCount(){
        return fragmentLightsOff.getCounterCount();
    }
    public Integer getOffCounterAll(){
        return fragmentLightsOff.getCounterAll();
    }
}
