package nightmares.smart.home.rest;

import java.io.IOException;

import nightmares.smart.home.database.LightEntity;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Light controller allow check light status, turn it on and off.
 * @author Dawid
 * @version 2
 * @since 2019-08-11
 */
public class LightController {
    private RetrofitApi retrofitApi;

    /**
     * Instantiates a new Light controller.
     */
    public LightController() {
        retrofitApi = RetrofitClient.get().create(RetrofitApi.class);
    }

    /**
     * Get {@link CallResponse} if device is available.
     *
     * @param lightEntity the {@link LightEntity}
     * @return the light response
     */
    public CallResponse get(LightEntity lightEntity) {
        try {
            Call<String> stringCall = retrofitApi.get(lightEntity.getAddress()+"/get?param="+ lightEntity.getLightID());
            Response<String> stringResponse = stringCall.execute();
            return new CallResponse(stringResponse.body());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Send switch on request.
     *
     * @param lightEntity the {@link LightEntity}
     * @return the light response {@link CallResponse}
     */
    public synchronized CallResponse on(LightEntity lightEntity) {
        try {
            Call<String> stringCall = retrofitApi.get(lightEntity.getAddress() + "/on?param="+ lightEntity.getLightID());
            Response<String> stringResponse = stringCall.execute();
            return new CallResponse(stringResponse.body());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Send switch off request.
     *
     * @param lightEntity the {@link LightEntity}
     * @return the light response {@link CallResponse}
     */
    public synchronized CallResponse off(LightEntity lightEntity) {
        try {
            Call<String> stringCall = retrofitApi.get(lightEntity.getAddress() + "/off?param="+ lightEntity.getLightID());
            Response<String> stringResponse = stringCall.execute();
            return new CallResponse(stringResponse.body());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
