package nightmares.smart.home.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * The interface Light dao.
 *
 * @author Dawid
 * @version 3
 * @since 2019-08-10
 */
@Dao
public interface LightDAO {
    //Light Entity

    /**
     * Save single {@link LightEntity}.
     *
     * @param light the light
     */
    @Insert
    long save(LightEntity light);

    /**
     * Update single {@link LightEntity}.
     *
     * @param light the light
     */
    @Update
    int update(LightEntity light);

    /**
     * Remove single {@link LightEntity}.
     *
     * @param light the light
     */
    @Delete
    int remove(LightEntity light);

    /**
     * List of all{@link LightEntity}.
     *
     * @return the list
     */
    @Query("Select * from LightEntity")
    List<LightEntity> all();

    /**
     * List of all{@link LightEntity} ordered by usage.
     *
     * @return the list
     */
    @Query("Select * from LightEntity order by usageCount DESC")
    List<LightEntity> allByUsage();

    //GroupLightEntity
    @Insert
    long save(GroupLightEntity groupLightEntity);

    @Update
    int update(GroupLightEntity groupLightEntity);

    @Delete
    int remove(GroupLightEntity groupLightEntity);

    @Query("Select * from GroupLightEntity")
    List<GroupLightEntity> allGroup();

    //GroupLightEntity
    @Insert
    long save(LightJoinGroup lightJoinGroup);

    @Update
    int update(LightJoinGroup lightJoinGroup);

    @Delete
    int remove(LightJoinGroup lightJoinGroup);

    @Query("Select * from LightEntity l inner join LightJoinGroup g on l.id=g.lightId where g.groupId=:gId order by l.usageCount")
    List<LightEntity> allJoin(final Integer gId);

    @Query("delete from LightJoinGroup where LightJoinGroup.groupId=:gId")
    int removeJoin(final Integer gId);
}
