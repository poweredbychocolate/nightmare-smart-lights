package nightmares.smart.home.activities.light;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import nightmares.smart.home.R;
import nightmares.smart.home.activities.MainActivity;
import nightmares.smart.home.adapters.light.LightAdapter;
import nightmares.smart.home.database.LightDAO;
import nightmares.smart.home.database.LocalDatabase;

/**
 * FragmentSwitches display list witch switches, allow turn it on and off by use {@link View#setOnLongClickListener(View.OnLongClickListener)}.
 */
public class FragmentSwitches extends Fragment {
    private MainActivity mainActivity;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipe;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_switches,container,false);
        mainActivity= (MainActivity) getActivity();
        swipe = view.findViewById(R.id.switches_swipe_refresh);
        recyclerView = view.findViewById(R.id.switches_list_view);
        swipe.setOnRefreshListener(() -> {
            reload();
            swipe.setRefreshing(false);
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setLayoutManager(new GridLayoutManager(mainActivity,2));
        recyclerView.setAdapter(new LightAdapter(LocalDatabase.get(getContext()).lightDAO(), mainActivity));
    }

    /**
     * Reload fragment {@link LightAdapter}.
     */
    public void reload(){
        if (recyclerView.getAdapter() != null) {
            ((LightAdapter) recyclerView.getAdapter()).refreshAdapter();
        }
    }
}
