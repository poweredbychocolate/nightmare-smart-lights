package nightmares.smart.home.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * The type Light join group.
 *
 * @author Dawid
 * @version 1
 * @since 2019-09-16
 */
@Entity(primaryKeys = {"groupId", "lightId"}, foreignKeys = {@ForeignKey(entity = LightEntity.class, parentColumns = "id", childColumns = "lightId"),
        @ForeignKey(entity = GroupLightEntity.class, parentColumns = "id", childColumns = "groupId")})
public class LightJoinGroup {
    @NonNull
    private Integer groupId;
    @NonNull
    private Integer lightId;

    /**
     * Instantiates a new Light join group.
     */
    public LightJoinGroup() {
    }

    /**
     * Instantiates a new Light join group.
     *
     * @param groupId the group id
     * @param lightId the light id
     */
    @Ignore
    public LightJoinGroup(@NonNull Integer groupId, @NonNull Integer lightId) {
        this.groupId = groupId;
        this.lightId = lightId;
    }

    /**
     * Gets group id.
     *
     * @return the group id
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * Sets group id.
     *
     * @param groupId the group id
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * Gets light id.
     *
     * @return the light id
     */
    public Integer getLightId() {
        return lightId;
    }

    /**
     * Sets light id.
     *
     * @param lightId the light id
     */
    public void setLightId(Integer lightId) {
        this.lightId = lightId;
    }

    @Override
    public String toString() {
        return "LightJoinGroup{" +
                "groupId=" + groupId +
                ", lightId=" + lightId +
                '}';
    }
}
