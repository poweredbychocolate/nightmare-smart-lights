package nightmares.smart.home.adapters.light;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nightmares.smart.home.R;
import nightmares.smart.home.activities.MainActivity;
import nightmares.smart.home.activities.light.FragmentLightsOn;
import nightmares.smart.home.activities.light.GroupLightAdapter;
import nightmares.smart.home.database.GroupLightEntity;
import nightmares.smart.home.database.LightDAO;
import nightmares.smart.home.database.LightEntity;
import nightmares.smart.home.database.LocalDatabase;
import nightmares.smart.home.rest.CallResponse;
import nightmares.smart.home.rest.LightController;

/**
 * Adapter for {@link FragmentLightsOn}.
 * @author Dawid
 * @version 2
 * @since 2019-08-13
 */
public class LightsOnAdapter extends RecyclerView.Adapter<LightHolder> {
    private LightDAO lightDAO;
    private List<GroupLightEntity> groups;
    private MainActivity mainActivity;
    private LightController lightController;
    private LightSwitcher switcher;
    private LightSwitchsReader reader;

    /**
     * Instantiates a new Auto on adapter.
     *
     * @param mainActivity the main activity
     */
    public LightsOnAdapter(MainActivity mainActivity) {
        this.groups = new ArrayList<>();
        this.lightDAO = LocalDatabase.get(mainActivity).lightDAO();
        this.mainActivity = mainActivity;
        lightController = new LightController();
        start();
    }


    @NonNull
    @Override
    public LightHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_light_card, viewGroup, false);
        return new LightHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LightHolder lightHolder, int i) {
        GroupLightEntity group = groups.get(i);
        lightHolder.getName().setText(group.getName());
        if (group.getList() != null && group.getList().size() > 0) {
            lightHolder.getStrip().setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.lightUnknown));
        } else {
            lightHolder.getStrip().setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.lightOffline));
        }
        lightHolder.getView().setOnLongClickListener(v -> {
            switcher = new LightSwitcher(group, lightHolder);
            switcher.execute();
            return true;
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
        View infoDialogView = mainActivity.getLayoutInflater().inflate(R.layout.dialog_group_info, null);
        ListView listView = infoDialogView.findViewById(R.id.d_group_info_list);
        if (group.getList() != null && group.getList().size() > 0) {
            listView.setAdapter(new GroupLightAdapter(mainActivity, group.getList()));
        }
        builder.setView(infoDialogView);
        builder.setCancelable(true).setPositiveButton(mainActivity.getString(R.string.dialog_group_remove), (dialog, which) -> new RemoveGroup(group).execute());
        // builder.setNegativeButton(mainActivity.getString(R.string.dialog_cancel), ( dialog, id) -> dialog.cancel());
        AlertDialog infoDialog = builder.create();
        lightHolder.getView().setOnClickListener(v -> infoDialog.show());
    }

    @Override
    public int getItemCount() {
        return groups != null ? groups.size() : 0;
    }
    ////


    /**
     * Read {@link LightEntity} list using {@link AsyncTask}.
     */
    class LightSwitchsReader extends AsyncTask<Void, GroupLightEntity, Void> {
        int counter;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mainActivity.showProgressBar();
            groups.clear();
            counter=0;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (GroupLightEntity entity : lightDAO.allGroup()) {
                if(isCancelled())break;
                entity.setList(lightDAO.allJoin(entity.getId()));
                publishProgress(entity);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(GroupLightEntity... values) {
            super.onProgressUpdate(values);
             groups.addAll(Arrays.asList(values));
            notifyDataSetChanged();
        }

        @Override
        protected void onPostExecute(Void lsVoid) {
            super.onPostExecute(lsVoid);
              mainActivity.hideProgressBar();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            notifyDataSetChanged();
            mainActivity.hideProgressBar();
        }
    }

    /**
     * The type Light switcher.
     */
    class LightSwitcher extends AsyncTask<Void, Integer, Boolean> {
        private final GroupLightEntity groupEntity;
        private final LightHolder holder;

        public LightSwitcher(GroupLightEntity entity, LightHolder lightHolder) {
            this.groupEntity = entity;
            this.holder = lightHolder;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mainActivity.showProgressBar();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                int c = 0;
                for (LightEntity light : groupEntity.getList()) {
                    if (isCancelled()) break;
                    CallResponse response = lightController.on(light);
                    if (response != null && response.getValue().equals(CallResponse.SUCCSESS)) {
                        c++;
                    }
                }
                return c > 0;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean status) {
            super.onPostExecute(status);
            if (status) {
                holder.getStrip().setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.lightOn));
            } else {
                holder.getStrip().setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.lightOffline));
            }
            mainActivity.hideProgressBar();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            notifyDataSetChanged();
            mainActivity.hideProgressBar();
        }
    }

    /**
     * The type Light switcher.
     */
    class RemoveGroup extends AsyncTask<Void, Void, Boolean> {
        private GroupLightEntity entity;

        private RemoveGroup(GroupLightEntity entity) {
            this.entity = entity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mainActivity.showProgressBar();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                lightDAO.removeJoin(entity.getId());
                entity.setList(null);
                lightDAO.remove(entity);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean status) {
            super.onPostExecute(status);
            if (status) {
                groups.remove(entity);
                notifyDataSetChanged();
            }
            mainActivity.hideProgressBar();
        }
    }

    /**
     * Start the turn off task.
     */
    public void start() {
        if (reader != null && !reader.isCancelled()) reader.cancel(true);
        reader = new LightSwitchsReader();
        reader.execute();
    }

    /**
     * Cancel task.
     */
    public void stop() {
        if (switcher != null && !switcher.isCancelled()) {
            switcher.cancel(true);
        }
        if (reader != null && !reader.isCancelled()) {
            reader.cancel(true);
        }
    }
}



