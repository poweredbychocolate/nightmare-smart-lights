# nightmare smart lights

Smart lights based on REST API for Arduino.
Arduino and ESP modules programmed using Arduino IDE.
- aRest Library - RESTful environment for Arduino.
- Javacord - An easy to use multithreaded library for creating Discord bots in Java.
- Retrofit - A type-safe HTTP client for Android and Java.

# what's work :
 * light.ino - 2 buttons and 2 relays
 * nightmares-discord-bot - command controla via Discord channel,
     virtual switches(add, remove, display list, switch on, switch off),
     switches groups (add, remove, display list, switch on, switch off).
 * nightmares-android - android client application, create virtual switch, 
     turn on and turn off by long press light card, define group, switch on or switch off selected group,
     allow turn on or turn off all virtual switches also.

# new function in discord-gate branch
 * command file mapper for nightmares-discord-bot - standard command and text can be replaced by custom set provides json file name as application param.
 * nightmares-discord-gate - A simple program that checks Discord channel if a new message with a Rest request arrive,
     executed it and return module response as next message.
 * nightmares-android -increased minSdkVersion to 26, required to use Javacord library. 
     Added screen for configure Discord gate connection and call a Rest request via gate if module is not available directly. 
 
# in progress :
 * nothing, project discontinued - in practice REST API for Arduino is not a good choice for communication.
   ideas can be used in mitsukurina smart home solutions.