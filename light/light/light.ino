/*
   Light Switches

   Version : Prototype-002
   Created by : Dawid Brelak
   Date : 03.09.2018

   REST controlled light control module.
 */

// Rest and esp library
#include <ESP8266WiFi.h>
#include <aREST.h>

// WiFi parameters
const char* ssid = "ssid";
const char* password = "password";


/*
   Pin definition
   D0 (GPOI16) -First button
   D1 (GPOI05) - Second button
   D5 (GPOI14) -First relay
   D6 (GPOI12) - Second relay
*/
const int SWITCH1 = 5;
const int SWITCH2 = 16;
const int RELAY1 =  12;
const int RELAY2 =  14;

// interval for buttons
const unsigned long INTERVAL = 500;

// Hardware state variables
bool relay1State = LOW;
bool relay2State = LOW;
bool switch1State = LOW;
bool switch2State = LOW;

// millis for button
unsigned long currentMillis = 0;
unsigned long switch1Millis = 0;
unsigned long switch2Millis = 0;

// Create aREST instance
aREST rest = aREST();
// The port to listen for incoming TCP connections
const int LISTEN_PORT = 80;
// Create an instance of the server
WiFiServer server(LISTEN_PORT);

// Rest functions declaration
int onControl(String value);
int offControl(String value);
int getControl(String value);

void setup(void)
{
  // Give name & ID to the device
  rest.set_id("10");
  rest.set_name("Light Switches");

  Serial.begin(115200);

  // hardware setup
  pinMode(SWITCH1, INPUT);
  pinMode(SWITCH2, INPUT);
  pinMode(RELAY1, OUTPUT);
  pinMode(RELAY2, OUTPUT);
  digitalWrite(RELAY1, !relay1State);
  digitalWrite(RELAY2, !relay2State);

  // rest read varialbles
  rest.variable("light1", &relay1State);
  rest.variable("light2", &relay2State);
  // control function
  rest.function("on", onControl);
  rest.function("off", offControl);
  rest.function("get", getControl);

  // Connect to WiFi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Light Switches connected");

  // Start the server
  server.begin();
  Serial.println("Light Switches address");

  // Print the IP address
  Serial.println(WiFi.localIP());
}

void loop() {
  //Buttons state update
  currentMillis = millis();
  if (currentMillis - switch1Millis >= INTERVAL && digitalRead(SWITCH1)) {
    Serial.println("Switch 1");
    switch1State = !switch1State;
        if(switch1State==HIGH){
         digitalWrite(RELAY1, LOW);
         relay1State = !LOW;
    }
        if(switch1State==LOW){
         digitalWrite(RELAY1,HIGH );
         relay1State = !HIGH;
    }
    switch1Millis = currentMillis;
  }
  if (currentMillis - switch2Millis >= INTERVAL && digitalRead(SWITCH2)) {
    Serial.println("Switch 2");
    switch2State = !switch2State;
    if(switch2State==HIGH){
         digitalWrite(RELAY2, LOW);
         relay2State = !LOW;
    }
        if(switch2State==LOW){
         digitalWrite(RELAY2,HIGH );
         relay2State = !HIGH;
    }
    switch2Millis = currentMillis;
  }

  // Handle REST calls
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  while (!client.available()) {
    delay(10);
  }
  rest.handle(client);

}

// turn on rest function
int onControl(String value) {
  Serial.println("Turn on"+value);
  if (value=="0") {
    digitalWrite(RELAY1, LOW);
    relay1State = !LOW;
    switch1State = HIGH;
    return 0;
  }
  if (value=="1") {
    digitalWrite(RELAY2, LOW);
    relay2State = !LOW;
    switch2State = HIGH;
    return 0;
  }
  return -100;
}
// turn off rest function
int offControl(String value) {
Serial.println("Turn Off"+ value);
  if (value=="0") {
    digitalWrite(RELAY1, HIGH);
    relay1State = !HIGH;
    switch1State = LOW;
    return 0;
  }
  if (value=="1") {
    digitalWrite(RELAY2, HIGH);
    relay2State = !HIGH;
    switch2State = LOW;
    return 0;
  }
  return -100;
}
int getControl(String value) {
Serial.println("Get State"+value);
    if (value=="0") {
    return relay1State;
  }
  if (value=="1") {
    return relay2State;
  }
  return -100;
}
