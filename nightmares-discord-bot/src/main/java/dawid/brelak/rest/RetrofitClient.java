package dawid.brelak.rest;

import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * {@link Retrofit} configuration
 * @version 1
 * @since 2019-08-11
 */
class RetrofitClient {
    private static Retrofit retrofit;

    static Retrofit get() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl("http://localhost")
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
