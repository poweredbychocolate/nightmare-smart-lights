package dawid.brelak.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dawid.brelak.model.GroupLightEntity;
import dawid.brelak.model.LightEntity;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DataService {
    private Map<String, LightEntity> lightMap;
    private Map<String, GroupLightEntity> groupMap;
    private final String lightMapFileName = "lights.json";
    private final String groupMapFileName = "light-groups.json";
    private Boolean lightChanged;
    private ObjectMapper mapper;
    private ScheduledExecutorService executor;

    public DataService() {
        this.mapper = new ObjectMapper();
        this.executor = Executors.newScheduledThreadPool(2);
        this.lightMap = readLightMap();
        this.groupMap = readGroupMap();
        this.executor.scheduleAtFixedRate(() -> {
            if (lightChanged) {
                saveLightMap();
                lightChanged = false;
            }
        }, 1, 1, TimeUnit.MINUTES);
    }

    public void close() {
        if (!executor.isShutdown()) executor.shutdown();
        saveLightMap();
        saveGroupMap();
    }

    public void setUsage(String key, Long number) {
        if (lightMap.containsKey(key)) {
            LightEntity entity=lightMap.get(key);
            entity.setUsageCount(number);
            lightChanged = true;
        }
    }

    //// Lights
    public boolean save(LightEntity lightEntity) {
        if (!lightMap.containsKey(lightEntity.getName())) {
            lightMap.put(lightEntity.getName(), lightEntity);
            executor.execute(this::saveLightMap);
            return true;
        }
        return false;
    }

    public boolean removeByKey(String lightEntityName) {
        if (lightMap.containsKey(lightEntityName)) {
            lightMap.remove(lightEntityName);
            executor.execute(this::saveLightMap);
            return true;
        }
        return false;
    }

    public Optional<LightEntity> getByKey(String lightEntityName) {
        return Optional.ofNullable(lightMap.get(lightEntityName));
    }

    public List<LightEntity> lightEntityList() {
        return new ArrayList<>(lightMap.values());
    }

    public boolean contains(String key) {
        return lightMap.containsKey(key);
    }

    private void saveLightMap() {
        try {
            mapper.writeValue(new File(lightMapFileName), lightMap);
            System.out.println("[SAVE] Save light list successfully");
        } catch (IOException e) {
            System.out.println("[ERROR] - Cannot save light list");
//            e.printStackTrace();
        }
    }

    private Map<String, LightEntity> readLightMap() {
        System.out.println("[LOAD] Loading light list, please wait");
        try {
            Map<String, LightEntity> map = mapper.readValue(new File(lightMapFileName), new TypeReference<Map<String, LightEntity>>() {
            });
            System.out.println("[LOAD] Load light list successfully");
            return map;
        } catch (IOException e) {
            System.out.println("[ERROR] - Cannot load light list");
//            e.printStackTrace();
            return new TreeMap<>();
        }
    }

    /// Lights Group
    public boolean save(GroupLightEntity entity) {
        if (!groupMap.containsKey(entity.getName())) {
            groupMap.put(entity.getName(), entity);
            executor.execute(this::saveGroupMap);
            return true;
        }
        return false;
    }

    public boolean removeGroupByKey(String groupName) {
        if (groupMap.containsKey(groupName)) {
            groupMap.remove(groupName);
            executor.execute(this::saveGroupMap);
            return true;
        }
        return false;
    }

    public Optional<GroupLightEntity> getGroupByKey(String groupName) {
        return Optional.ofNullable(groupMap.get(groupName));
    }

    public List<GroupLightEntity> groupLightEntityList() {
        return new ArrayList<>(groupMap.values());
    }

    private void saveGroupMap() {
        try {
            mapper.writeValue(new File(groupMapFileName), groupMap);
            System.out.println("[SAVE] Save group list successfully");
        } catch (IOException e) {
            System.out.println("[ERROR] - Cannot save group list");
//            e.printStackTrace();
        }
    }

    private Map<String, GroupLightEntity> readGroupMap() {
        System.out.println("[LOAD] Loading group list, please wait");
        try {
            Map<String, GroupLightEntity> map = mapper.readValue(new File(groupMapFileName), new TypeReference<Map<String, GroupLightEntity>>() {
            });
            System.out.println("[LOAD] Load group list successfully");
            return map;
        } catch (IOException e) {
            System.out.println("[ERROR] - Cannot load group list");
//            e.printStackTrace();
            return new TreeMap<>();
        }
    }


}
