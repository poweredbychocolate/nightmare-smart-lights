package dawid.brelak.services;

import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.listener.message.MessageCreateListener;

public class DiscordService {
    private DiscordApi discordApi;

    public DiscordService(String token) {
        discordApi = new DiscordApiBuilder().setToken(token).login().join();

    }

    public void addMessageCreateListener(MessageCreateListener messageCreateListener){
        discordApi.addMessageCreateListener(messageCreateListener);
    }
    public void close() {
        discordApi.disconnect();
    }
}
