package dawid.brelak;

import dawid.brelak.lang.mapper.LangMapper;
import dawid.brelak.lang.mapper.NoMapper;
import dawid.brelak.rest.LightController;
import dawid.brelak.services.DataService;
import dawid.brelak.services.DiscordService;
import java.util.Optional;
import java.util.Scanner;

/**
 *
 */
public class App {
    private static DiscordService discordService;
    private static DataService dataService;

    public static void main(String[] args) {
        Optional<String> botToken = Optional.ofNullable(args.length > 0 ? args[0] : null);
        Optional<String> mapperName = Optional.ofNullable(args.length > 1 ? args[1] : null);

        if (botToken.isPresent()) {
            System.out.println("Nightmares Discord Bot");
            System.out.println("Connecting with a token : " + botToken.get());
            LangMapper langMapper;
            if (mapperName.isPresent()) {
                //TODO add other mapper
                langMapper = new NoMapper();
            } else {
                langMapper = new NoMapper();
            }
            discordService = new DiscordService(botToken.get());
            dataService = new DataService();
            LightController lightController = new LightController();
            discordService.addMessageCreateListener(new CommandListener(langMapper,dataService, lightController));
            System.out.println("Load command mapper : " + langMapper.getClass().getSimpleName());
            System.out.println("Nightmares Discord Bot connected");
        } else {
            System.out.println("Nightmares Discord Bot cannot connect to server");
            System.out.println("Please check your token and try again");
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter exit to close");
        while (!scanner.next().equalsIgnoreCase("exit")) {
            System.out.println("Enter exit to close");
        }
        if (discordService != null) discordService.close();
        if (dataService != null) dataService.close();
        System.out.println("Nightmares Discord Bot was closed see you later");
    }

}
