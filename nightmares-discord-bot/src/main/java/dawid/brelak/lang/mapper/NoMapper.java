package dawid.brelak.lang.mapper;

import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.message.MessageDecoration;
import java.util.Properties;

public class NoMapper implements LangMapper {
    private final MessageBuilder[] messageBuilders;
    private final Properties properties;

    public NoMapper() {
        messageBuilders = new MessageBuilder[]{nightmaresCommands(), nightmaresGroupCommands()};
        properties = mapperProperties();
    }

    @Override
    public String mapComm(String command) {
        if (command.toLowerCase().startsWith("nightmares")) return command.replaceFirst("nightmares", "nm");
        return command;
    }

    private Properties mapperProperties() {
        Properties properties = new Properties();
        //Welcome message property
        properties.setProperty("bot.name", "Nightmares Smart Home Discord Bot");
        properties.setProperty("bot.name.info", "Part of Nightmares Smart Home System");
        properties.setProperty("bot.name.version", "Version 0.2");
        properties.setProperty("bot.name.message", "Enter nm help or nightmares help to see available command");
        properties.setProperty("wait", "Connecting...");
        properties.setProperty("response.fail", "unknown connection error");
        properties.setProperty("light.new.no.data", "Incorrect command, light data not found");
        properties.setProperty("light.new.incorrect.data", "Incorrect command, light data is not correct");
        properties.setProperty("light.new.exist", " - label is currently used, select other name");
        properties.setProperty("group.new.no.data", "Incorrect command, group data not found");
        properties.setProperty("group.new.incorrect.data", "Incorrect command, group data is not correct");
        properties.setProperty("group.new.exist", "Name of group is currently used, select other name");
        properties.setProperty("light.new.success", "was added successfully");
        properties.setProperty("group.new.success", "was added successfully");
        properties.setProperty("light.remove.no.selected", "Name of light was not entered");
        properties.setProperty("group.remove.no.selected", "Name of group was not entered");
        properties.setProperty("group.no.selected", "Name of group was not entered");
        properties.setProperty("light.remove.no.found", "Name of light was not found");
        properties.setProperty("group.remove.no.found", "Name of group was not found");
        properties.setProperty("light.remove.success", "was removed successfully");
        properties.setProperty("group.remove.success", "was removed successfully");
        properties.setProperty("light.list.empty", "Light switches list is empty");
        properties.setProperty("group.list.empty", "Group list is empty");
        properties.setProperty("light.list", "List of available switches");
        properties.setProperty("group.list", "List of available group");
        properties.setProperty("group.light.label.not.found", "skip, light label was not found");
        properties.setProperty("check.no.selected", "Label of light was not entered");
        properties.setProperty("light.no.selected", "Label of light was not entered");
        properties.setProperty("check.on.exist", "was not found");
        properties.setProperty("group.no.exist", "was not found");
        properties.setProperty("light.no.exist", "was not found");
        properties.setProperty("group.checking", "Checking group");
        properties.setProperty("group.turning.on", "Turning on group.");
        properties.setProperty("group.turning.off", "Turning off group");
        properties.setProperty("response.offline", "is offline");
        properties.setProperty("response.off", "is turned off");
        properties.setProperty("response.on", "is turned on");
        properties.setProperty("unknown.command", "Unknown command");
        properties.setProperty("on.success", "was turned on");
        properties.setProperty("off.success", "was turned off");


        return properties;
    }

    @Override
    public void welcomeMessage(TextChannel channel) {
        new MessageBuilder()
                .append(properties.getProperty("bot.name"), MessageDecoration.BOLD).appendNewLine()
                .append(properties.getProperty("bot.name.info"), MessageDecoration.ITALICS).appendNewLine()
                .append(properties.getProperty("bot.name.version"), MessageDecoration.ITALICS).appendNewLine().appendNewLine()
                .append(properties.getProperty("bot.name.message")).appendNewLine()
                .send(channel);
    }

    @Override
    public void commandList(TextChannel channel) {
        messageBuilders[0].send(channel);
        messageBuilders[1].send(channel);
    }

    @Override
    public String response(String type) {
        switch (type) {
            case LangMapper.LIGHT_NEW_NO_DATA:
                return properties.getProperty("light.new.no.data");
            case LangMapper.LIGHT_NEW_INCORRECT_DATA:
                return properties.getProperty("light.new.incorrect.data");
            case LangMapper.LIGHT_NEW_EXIST:
                return properties.getProperty("light.new.exist");
            case LangMapper.GROUP_NEW_NO_DATA:
                return properties.getProperty("group.new.no.data");
            case LangMapper.GROUP_NEW_INCORRECT_DATA:
                return properties.getProperty("group.new.incorrect.data");
            case LangMapper.GROUP_NEW_EXIST:
                return properties.getProperty("group.new.exist");
            case LangMapper.LIGHT_NEW_SUCCESS:
                return properties.getProperty("light.new.success");
            case LangMapper.GROUP_NEW_SUCCESS:
                return properties.getProperty("group.new.success");
            case LangMapper.LIGHT_REMOVE_NO_SELECTED:
                return properties.getProperty("light.remove.no.selected");
            case LangMapper.GROUP_REMOVE_NO_SELECTED:
                return properties.getProperty("group.remove.no.selected");
            case LangMapper.GROUP_NO_SELECTED:
                return properties.getProperty("group.no.selected");
            case LangMapper.LIGHT_REMOVE_NO_FOUND:
                return properties.getProperty("light.remove.no.found");
            case LangMapper.GROUP_REMOVE_NO_FOUND:
                return properties.getProperty("group.remove.no.found");
            case LangMapper.LIGHT_REMOVE_SUCCESS:
                return properties.getProperty("light.remove.success");
            case LangMapper.GROUP_REMOVE_SUCCESS:
                return properties.getProperty("group.remove.succes");
            case LangMapper.LIGHT_LIST_EMPTY:
                return properties.getProperty("light.list.empty");
            case LangMapper.GROUP_LIST_EMPTY:
                return properties.getProperty("group.list.empty");
            case LangMapper.LIGHT_LIST:
                return properties.getProperty("light.list");
            case LangMapper.GROUP_LIST:
                return properties.getProperty("group.list");
            case LangMapper.GROUP_LIGHT_LABEL_NOT_FOUND:
                return properties.getProperty("group.light.label.not.found");
            case LangMapper.CHECK_NO_SELECTED:
                return properties.getProperty("check.no.selected");
            case LangMapper.LIGHT_NO_SELECTED:
                return properties.getProperty("light.no.selected");
            case LangMapper.CHECK_NO_EXIST:
                return properties.getProperty("check.on.exist");
            case LangMapper.GROUP_NO_EXIST:
                return properties.getProperty("group.no.exist");
            case LangMapper.LIGHT_NO_EXIST:
                return properties.getProperty("light.no.exist");
            case LangMapper.GROUP_CHECKING:
                return properties.getProperty("group.checking");
            case LangMapper.GROUP_TURNING_ON:
                return properties.getProperty("group.turning.on");
            case LangMapper.GROUP_TURNING_OFF:
                return properties.getProperty("group.turning.off");
            case LangMapper.RESPONSE_OFFLINE:
                return properties.getProperty("response.offline");
            case LangMapper.RESPONSE_OFF:
                return properties.getProperty("response.off");
            case LangMapper.RESPONSE_ON:
                return properties.getProperty("response.on");
            case LangMapper.UNKNOWN_COMMAND:
                return properties.getProperty("unknown.command");
            case LangMapper.ON_SUCCESS:
                return properties.getProperty("on.success");
            case LangMapper.OFF_SUCCESS:
                return properties.getProperty("off.success");
            case LangMapper.RESPONSE_FAIL:
                return properties.getProperty("response.fail");
            case LangMapper.WAIT:
                return properties.getProperty("wait");
        }
        return " ";
    }

    private MessageBuilder nightmaresCommands() {
        MessageBuilder mB = new MessageBuilder();
        mB.append("Nightmares Smart Home Discord Bot", MessageDecoration.BOLD).appendNewLine();
        mB.append("Supported command list :", MessageDecoration.ITALICS).appendNewLine().appendNewLine();
        //Add light command
        mB.append("nm add <light switch connection> or nightmares add <light switch connection>",
                MessageDecoration.BOLD, MessageDecoration.ITALICS).appendNewLine();
        mB.append("Add new connection using light switch connection pattern - |connection name, connection address,light Id|",
                MessageDecoration.ITALICS).appendNewLine().append("For example :").appendNewLine();
        mB.append("nm add |Archangel, http://192.168.0.100, L1| - save connection called Archangel").appendNewLine().appendNewLine();
        //Remove light command
        mB.append("nm remove <name> or nightmares remove <name>",
                MessageDecoration.BOLD, MessageDecoration.ITALICS).appendNewLine();
        mB.append("remove switches connection by name",
                MessageDecoration.ITALICS).appendNewLine().append("For example :").appendNewLine();
        mB.append("nightmares remove bulb - remove connection called bulb, will be no longer available").appendNewLine().appendNewLine();
        //Light list command
        mB.append("nm list or nightmares list",
                MessageDecoration.BOLD, MessageDecoration.ITALICS).appendNewLine();
        mB.append("display labels for all connected light switches",
                MessageDecoration.ITALICS).appendNewLine().appendNewLine();
        //light check command
        mB.append("nm check <labels> or nightmares check<labels>",
                MessageDecoration.BOLD, MessageDecoration.ITALICS).appendNewLine();
        mB.append("check if switches are turned on or turned off, detect offline devices also",
                MessageDecoration.ITALICS).appendNewLine().append("For example :").appendNewLine();
        mB.append("nm check bulb - check single light").appendNewLine();
        mB.append("nm check bulb,bulb2,light1,l1 - check multiple lights").appendNewLine().appendNewLine();
        //Turn on command
        mB.append("nm on <labels> or nightmares on <labels>",
                MessageDecoration.BOLD, MessageDecoration.ITALICS).appendNewLine();
        mB.append("turn on all switches from list",
                MessageDecoration.ITALICS).appendNewLine().append("For example :").appendNewLine();
        mB.append("nm on bulb - turn on single light").appendNewLine();
        mB.append("nm on bulb,bulb2,light1,l1 - turn on multiple lights").appendNewLine().appendNewLine();
        //Turn off command
        mB.append("nm off <labels> or nightmares off <labels>",
                MessageDecoration.BOLD, MessageDecoration.ITALICS).appendNewLine();
        mB.append("turn off all switches from list",
                MessageDecoration.ITALICS).appendNewLine().append("For example :").appendNewLine();
        mB.append("nightmares off bulb - turn off single light").appendNewLine();
        mB.append("nm off bulb,bulb2,light1,l1 - turn off multiple lights").appendNewLine().appendNewLine();
        return mB;

    }

    private MessageBuilder nightmaresGroupCommands() {
        MessageBuilder mB = new MessageBuilder();
        //Add group command
        mB.append("\n nmg add <light group>",
                MessageDecoration.BOLD, MessageDecoration.ITALICS).appendNewLine();
        mB.append("Create new group for selected light, light group pattern - |group name+light list|",
                MessageDecoration.ITALICS).appendNewLine().append("For example :").appendNewLine();
        mB.append("nmg add |Room+L1,L2,L3,L4,L5| - create group with 5 light switches").appendNewLine().appendNewLine();
        //Remove group command
        mB.append("nmg remove <name>",
                MessageDecoration.BOLD, MessageDecoration.ITALICS).appendNewLine();
        mB.append("remove group by name",
                MessageDecoration.ITALICS).appendNewLine().append("For example :").appendNewLine();
        mB.append("nmg  remove Room - remove group with name Room").appendNewLine().appendNewLine();
        //Light group list command
        mB.append("nmg list",
                MessageDecoration.BOLD, MessageDecoration.ITALICS).appendNewLine();
        mB.append("display all group names",
                MessageDecoration.ITALICS).appendNewLine().appendNewLine();
        //group check command
        mB.append("nmg check <names>",
                MessageDecoration.BOLD, MessageDecoration.ITALICS).appendNewLine();
        mB.append("Check if all switches in a group or groups are turned on or turned off.",
                MessageDecoration.ITALICS).appendNewLine().append("For example :").appendNewLine();
        mB.append("nmg check bulb,light - check all switches in both groups").appendNewLine().appendNewLine();
        //Group turn on command
        mB.append("nmg on <names>",
                MessageDecoration.BOLD, MessageDecoration.ITALICS).appendNewLine();
        mB.append("turn on all switches from group or groups",
                MessageDecoration.ITALICS).appendNewLine().append("For example :").appendNewLine();
        mB.append("nmg on Room - turn on all light from Room group").appendNewLine().appendNewLine();
        //Group turn off command
        mB.append("nmg off <names>",
                MessageDecoration.BOLD, MessageDecoration.ITALICS).appendNewLine();
        mB.append("turn off all switches from group or groups",
                MessageDecoration.ITALICS).appendNewLine().append("For example :").appendNewLine();
        mB.append("nmg off bulb,light - turn off all light from both groups").appendNewLine().appendNewLine();
        return mB;
    }

}
