package dawid.brelak.lang.mapper;

import org.javacord.api.entity.channel.TextChannel;

public interface LangMapper {
    String LIGHT_NEW_NO_DATA = "A1";
    String LIGHT_NEW_INCORRECT_DATA = "A2";
    String LIGHT_NEW_EXIST = "A3";
    String LIGHT_NEW_SUCCESS = "A4";
    String LIGHT_REMOVE_NO_SELECTED = "A5";
    String LIGHT_REMOVE_NO_FOUND = "A6";
    String LIGHT_REMOVE_SUCCESS = "A7";
    String LIGHT_LIST_EMPTY = "A8";
    String LIGHT_LIST = "A9";
    String CHECK_NO_SELECTED = "A0";
    String CHECK_NO_EXIST = "B1";
    String RESPONSE_OFFLINE = "B2";
    String RESPONSE_OFF = "B3";
    String RESPONSE_ON = "B4";
    String UNKNOWN_COMMAND = "B5";
    String ON_SUCCESS = "B6";
    String OFF_SUCCESS = "B7";
    String RESPONSE_FAIL = "B8";
    String WAIT = "WAIT";
    String GROUP_NO_SELECTED = "B9";
    String LIGHT_NO_EXIST = "B0";
    String GROUP_NO_EXIST = "C1";
    String LIGHT_NO_SELECTED = "C2";
    String GROUP_LIST = "C3";
    String GROUP_LIST_EMPTY = "C4";
    String GROUP_REMOVE_SUCCESS = "C5";
    String GROUP_REMOVE_NO_FOUND = "C6";
    String GROUP_REMOVE_NO_SELECTED = "C7";
    String GROUP_NEW_SUCCESS = "C8";
    String GROUP_NEW_EXIST = "C9";
    String GROUP_NEW_INCORRECT_DATA = "C0";
    String GROUP_NEW_NO_DATA = "D1";
    String GROUP_LIGHT_LABEL_NOT_FOUND = "D2";
    String GROUP_CHECKING = "D3";
    String GROUP_TURNING_ON = "D4";
    String GROUP_TURNING_OFF = "D5";


    String mapComm(String command);

    void welcomeMessage(TextChannel channel);

    void commandList(TextChannel channel);

    String response(String type);
}
