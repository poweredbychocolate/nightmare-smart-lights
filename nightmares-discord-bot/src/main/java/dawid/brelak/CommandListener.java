package dawid.brelak;

import dawid.brelak.lang.mapper.LangMapper;
import dawid.brelak.model.GroupLightEntity;
import dawid.brelak.model.LightEntity;
import dawid.brelak.rest.CallResponse;
import dawid.brelak.rest.LightController;
import dawid.brelak.services.DataService;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.message.MessageDecoration;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CommandListener implements MessageCreateListener {
    private static final int MSG_LIMIT = 1980;
    private LangMapper langMapper;
    private DataService dataService;
    private LightController lightController;

    CommandListener(LangMapper langMapper, DataService dataService, LightController lightController) {
        this.langMapper = langMapper;
        this.dataService = dataService;
        this.lightController = lightController;
    }

    @Override
    public void onMessageCreate(MessageCreateEvent event) {
        if (event.getMessageAuthor().isRegularUser()) {
            String msg = langMapper.mapComm(event.getMessageContent());
            if (msg.equalsIgnoreCase("ping")) {
                event.getChannel().sendMessage("pong");
            } else if (msg.equalsIgnoreCase("time")) {
                event.getChannel().sendMessage(LocalDateTime.now().format(DateTimeFormatter.ofPattern("EEEE, dd MMMM yyyy HH:mm:ss")));
            } else if (msg.toLowerCase().startsWith("echo")) {
                int i = msg.indexOf(" ");
                String response = i == -1 ? "echo" : msg.substring(msg.indexOf(" "));
                event.getChannel().sendMessage(response);
            } else {
                if (msg.equalsIgnoreCase("nm")) {
                    langMapper.welcomeMessage(event.getChannel());
                } else if (msg.equalsIgnoreCase("nm help")) {
                    langMapper.commandList(event.getChannel());
                } else if (msg.toLowerCase().startsWith("nm add")) {
                    lightAdd(msg, event);
                } else if (msg.toLowerCase().startsWith("nm remove")) {
                    lightRemove(msg, event);
                } else if (msg.equalsIgnoreCase("nm list")) {
                    lightList(event);
                } else if (msg.toLowerCase().startsWith("nm check")) {
                    lightCheck(msg, event);
                } else if (msg.toLowerCase().startsWith("nm on")) {
                    lightOn(msg, event);
                } else if (msg.toLowerCase().startsWith("nm off")) {
                    lightOff(msg, event);
                } else if (msg.toLowerCase().startsWith("nmg add")) {
                    lightGroupAdd(msg, event);
                } else if (msg.toLowerCase().startsWith("nmg remove")) {
                    lightGroupRemove(msg, event);
                } else if (msg.equalsIgnoreCase("nmg list")) {
                    lightGroupList(event);
                } else if (msg.toLowerCase().startsWith("nmg check")) {
                    lightGroupCheck(msg, event);
                } else if (msg.toLowerCase().startsWith("nmg on")) {
                    lightGroupOn(msg, event);
                } else if (msg.toLowerCase().startsWith("nmg off")) {
                    lightGroupOff(msg, event);
                } else if (msg.startsWith("nm")) {
                    //unsupported command
                    event.getChannel().sendMessage(langMapper.response(LangMapper.UNKNOWN_COMMAND));
                }
            }
        }
    }

    private void lightOff(String msg, MessageCreateEvent event) {
        int i = msg.indexOf(" ", "nm off".length());
        if (i != -1) {
            Toast.wait(langMapper.response(LangMapper.WAIT), event.getChannel());
            String[] parts = msg.substring(i).split(",");
            for (String part : parts) {
                Optional<LightEntity> lightEntity = dataService.getByKey(part.trim());
                if (lightEntity.isPresent()) {
                    CallResponse response = lightController.off(lightEntity.get());
                    if (response == null) {
                        Toast.offline(lightEntity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_OFFLINE), event.getChannel());
                    } else {
                        if (response.getValue().equals(CallResponse.SUCCSESS)) {
                            Toast.success(lightEntity.get().getName() + " " + langMapper.response(LangMapper.OFF_SUCCESS), event.getChannel());
                        } else {
                            Toast.fail(lightEntity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_FAIL), event.getChannel());
                        }
                    }
                    dataService.setUsage(lightEntity.get().getName(), lightEntity.get().getUsageCount() + 1);
                } else {
                    Toast.info(part + " " + langMapper.response(LangMapper.LIGHT_NO_EXIST), event.getChannel());
                }
            }
        } else {
            Toast.info(langMapper.response(LangMapper.LIGHT_NO_SELECTED), event.getChannel());
        }
    }

    private void lightGroupOff(String msg, MessageCreateEvent event) {
        int i = msg.indexOf(" ", "nmg off".length());
        if (i != -1) {
            Toast.wait(langMapper.response(LangMapper.WAIT), event.getChannel());
            String[] parts = msg.substring(i).split(",");
            for (String part : parts) {
                Optional<GroupLightEntity> lightEntity = dataService.getGroupByKey(part.trim());
                if (lightEntity.isPresent()) {
                    Toast.info(langMapper.response(LangMapper.GROUP_TURNING_OFF) + " " + lightEntity.get().getName(), event.getChannel());
                    for (String label : lightEntity.get().getLightEntitiesKeys()) {
                        Optional<LightEntity> entity = dataService.getByKey(label);
                        if (entity.isPresent()) {
                            CallResponse response = lightController.off(entity.get());
                            if (response == null) {
                                Toast.offline(entity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_OFFLINE), event.getChannel());
                            } else {
                                if (response.getValue().equals(CallResponse.SUCCSESS)) {
                                    Toast.success(entity.get().getName() + " " + langMapper.response(LangMapper.OFF_SUCCESS), event.getChannel());
                                } else {
                                    Toast.fail(entity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_FAIL), event.getChannel());
                                }
                            }
                            dataService.setUsage(entity.get().getName(), entity.get().getUsageCount() + 1);
                        } else {
                            lightEntity.get().getLightEntitiesKeys().remove(part.trim());
                        }
                    }
                } else {
                    Toast.info(part + " " + langMapper.response(LangMapper.GROUP_NO_EXIST), event.getChannel());
                }
            }
        } else {
            Toast.info(langMapper.response(LangMapper.GROUP_NO_SELECTED), event.getChannel());
        }
    }


    private void lightOn(String msg, MessageCreateEvent event) {
        int i = msg.indexOf(" ", "nm on".length());
        if (i != -1) {
            Toast.wait(langMapper.response(LangMapper.WAIT), event.getChannel());
            String[] parts = msg.substring(i).split(",");
            for (String part : parts) {
                Optional<LightEntity> lightEntity = dataService.getByKey(part.trim());
                if (lightEntity.isPresent()) {
                    CallResponse response = lightController.on(lightEntity.get());
                    if (response == null) {
                        Toast.offline(lightEntity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_OFFLINE), event.getChannel());
                    } else {
                        if (response.getValue().equals(CallResponse.SUCCSESS)) {
                            Toast.success(lightEntity.get().getName() + " " + langMapper.response(LangMapper.ON_SUCCESS), event.getChannel());
                        } else {
                            Toast.fail(lightEntity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_FAIL), event.getChannel());
                        }
                    }
                    dataService.setUsage(lightEntity.get().getName(), lightEntity.get().getUsageCount() + 1);
                } else {
                    Toast.info(part + " " + langMapper.response(LangMapper.LIGHT_NO_EXIST), event.getChannel());
                }
            }
        } else {
            Toast.info(langMapper.response(LangMapper.LIGHT_NO_SELECTED), event.getChannel());
        }
    }

    private void lightGroupOn(String msg, MessageCreateEvent event) {
        int i = msg.indexOf(" ", "nmg on".length());
        if (i != -1) {
            Toast.wait(langMapper.response(LangMapper.WAIT), event.getChannel());
            String[] parts = msg.substring(i).split(",");
            for (String part : parts) {
                Optional<GroupLightEntity> lightEntity = dataService.getGroupByKey(part.trim());
                if (lightEntity.isPresent()) {
                    Toast.info(langMapper.response(LangMapper.GROUP_TURNING_ON) + " " + lightEntity.get().getName(), event.getChannel());
                    for (String label : lightEntity.get().getLightEntitiesKeys()) {
                        Optional<LightEntity> entity = dataService.getByKey(label);
                        if (entity.isPresent()) {
                            CallResponse response = lightController.on(entity.get());
                            if (response == null) {
                                Toast.offline(entity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_OFFLINE), event.getChannel());
                            } else {
                                if (response.getValue().equals(CallResponse.SUCCSESS)) {
                                    Toast.success(entity.get().getName() + " " + langMapper.response(LangMapper.ON_SUCCESS), event.getChannel());
                                } else {
                                    Toast.fail(entity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_FAIL), event.getChannel());
                                }
                            }
                            dataService.setUsage(entity.get().getName(), entity.get().getUsageCount() + 1);
                        } else {
                            lightEntity.get().getLightEntitiesKeys().remove(part.trim());
                        }
                    }
                } else {
                    Toast.info(part + " " + langMapper.response(LangMapper.GROUP_NO_EXIST), event.getChannel());
                }
            }
        } else {
            Toast.info(langMapper.response(LangMapper.GROUP_NO_SELECTED), event.getChannel());
        }
    }

    private void lightCheck(String msg, MessageCreateEvent event) {
        int i = msg.indexOf(" ", "nm check".length());
        if (i != -1) {
            Toast.wait(langMapper.response(LangMapper.WAIT), event.getChannel());
            String[] parts = msg.substring(i).split(",");
            for (String part : parts) {
                Optional<LightEntity> lightEntity = dataService.getByKey(part.trim());
                if (lightEntity.isPresent()) {
                    CallResponse response = lightController.get(lightEntity.get());
                    if (response == null) {
                        Toast.offline(lightEntity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_OFFLINE), event.getChannel());
                    } else {
                        if (response.getValue().equals(CallResponse.ON)) {
                            Toast.on(lightEntity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_ON), event.getChannel());
                        } else {
                            Toast.off(lightEntity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_OFF), event.getChannel());
                        }
                    }
                } else {
                    Toast.info(part + " " + langMapper.response(LangMapper.LIGHT_NO_EXIST), event.getChannel());
                }
            }
        } else {
            Toast.info(langMapper.response(LangMapper.LIGHT_NO_SELECTED), event.getChannel());
        }
    }

    private void lightGroupCheck(String msg, MessageCreateEvent event) {
        int i = msg.indexOf(" ", "nmg check".length());
        if (i != -1) {
            Toast.wait(langMapper.response(LangMapper.WAIT), event.getChannel());
            String[] parts = msg.substring(i).split(",");
            for (String part : parts) {
                Optional<GroupLightEntity> lightEntity = dataService.getGroupByKey(part.trim());
                if (lightEntity.isPresent()) {
                    Toast.info(langMapper.response(LangMapper.GROUP_CHECKING) + " " + lightEntity.get().getName(), event.getChannel());
                    for (String label : lightEntity.get().getLightEntitiesKeys()) {
                        Optional<LightEntity> entity = dataService.getByKey(label);
                        if (entity.isPresent()) {
                            CallResponse response = lightController.get(entity.get());
                            if (response == null) {
                                Toast.offline(entity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_OFFLINE), event.getChannel());
                            } else {
                                if (response.getValue().equals(CallResponse.ON)) {
                                    Toast.on(entity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_ON), event.getChannel());
                                } else {
                                    Toast.off(entity.get().getName() + " " + langMapper.response(LangMapper.RESPONSE_OFF), event.getChannel());
                                }
                            }
                        } else {
                            lightEntity.get().getLightEntitiesKeys().remove(part.trim());
                        }
                    }
                } else {
                    Toast.info(part + " " + langMapper.response(LangMapper.GROUP_NO_EXIST), event.getChannel());
                }
            }
        } else {
            Toast.info(langMapper.response(LangMapper.GROUP_NO_SELECTED), event.getChannel());
        }
    }

    private void lightList(MessageCreateEvent event) {
        List<LightEntity> list = dataService.lightEntityList();
        if (!list.isEmpty()) {
            MessageBuilder builder = new MessageBuilder();
            builder.append(langMapper.response(LangMapper.LIGHT_LIST), MessageDecoration.ITALICS).appendNewLine().appendNewLine();
            int i = 1;
            for (LightEntity entity : list) {
                if (builder.getStringBuilder().length() + entity.getName().length() < MSG_LIMIT) {
                    builder.append(i++).append(". ").append(entity.getName()).appendNewLine();
                } else {
                    builder.send(event.getChannel());
                    builder = new MessageBuilder();
                }
            }
            builder.send(event.getChannel());
        } else {
            Toast.info(langMapper.response(LangMapper.LIGHT_LIST_EMPTY), event.getChannel());
        }
    }

    private void lightGroupList(MessageCreateEvent event) {
        List<GroupLightEntity> list = dataService.groupLightEntityList();
        if (!list.isEmpty()) {
            MessageBuilder builder = new MessageBuilder();
            builder.append(langMapper.response(LangMapper.GROUP_LIST), MessageDecoration.ITALICS).appendNewLine().appendNewLine();
            int i = 1;
            for (GroupLightEntity entity : list) {
                if (builder.getStringBuilder().length() + entity.getName().length() < MSG_LIMIT) {
                    builder.append(i++).append(". ").append(entity.getName()).appendNewLine();
                } else {
                    builder.send(event.getChannel());
                    builder = new MessageBuilder();
                }
            }
            builder.send(event.getChannel());
        } else {
            Toast.info(langMapper.response(LangMapper.GROUP_LIST_EMPTY), event.getChannel());
        }
    }

    private void lightRemove(String msg, MessageCreateEvent event) {
        int i = msg.indexOf(" ", "nm remove".length());
        if (i != -1) {
            String name = msg.substring(i).trim();
            if (dataService.removeByKey(name)) {
                Toast.success(name + " " + langMapper.response(LangMapper.LIGHT_REMOVE_SUCCESS), event.getChannel());
            } else {
                Toast.fail(langMapper.response(LangMapper.LIGHT_REMOVE_NO_FOUND), event.getChannel());
            }
        } else {
            Toast.info(langMapper.response(LangMapper.LIGHT_REMOVE_NO_SELECTED), event.getChannel());
        }
    }

    private void lightGroupRemove(String msg, MessageCreateEvent event) {
        int i = msg.indexOf(" ", "nmg remove".length());
        if (i != -1) {
            String name = msg.substring(i).trim();
            if (dataService.removeGroupByKey(name)) {
                Toast.success(name + " " + langMapper.response(LangMapper.GROUP_REMOVE_SUCCESS), event.getChannel());
            } else {
                Toast.fail(langMapper.response(LangMapper.GROUP_REMOVE_NO_FOUND), event.getChannel());
            }
        } else {
            Toast.info(langMapper.response(LangMapper.GROUP_REMOVE_NO_SELECTED), event.getChannel());
        }
    }

    private void lightAdd(String msg, MessageCreateEvent event) {
        int i = msg.indexOf('|');
        if (i != -1) {
            String[] parts = msg.substring(i).replace("|", "").split(",");
            if (parts.length == 3) {
                LightEntity lightEntity = new LightEntity();
                lightEntity.setName(parts[0].trim());
                lightEntity.setAddress(parts[1].trim());
                lightEntity.setLightID(parts[2].trim());
                boolean s = dataService.save(lightEntity);
                if (s) {
                    Toast.success(lightEntity.getName() + " " + langMapper.response(LangMapper.LIGHT_NEW_SUCCESS), event.getChannel());
                } else {
                    Toast.fail(lightEntity.getName() + " " + langMapper.response(LangMapper.LIGHT_NEW_EXIST), event.getChannel());
                }
            } else {
                Toast.info(langMapper.response(LangMapper.LIGHT_NEW_INCORRECT_DATA), event.getChannel());
            }
        } else {
            Toast.info(langMapper.response(LangMapper.LIGHT_NEW_NO_DATA), event.getChannel());
        }
    }

    private void lightGroupAdd(String msg, MessageCreateEvent event) {
        int i = msg.indexOf('|');
        int ii = msg.indexOf("+");
        if (i != -1 && ii != -1) {
            String name = msg.substring(i, ii).replace("|", "");
            String[] parts = msg.substring(ii).replace("|", "").replace("+", "").split(",");
            if (parts.length > 0) {
                GroupLightEntity entity = new GroupLightEntity();
                entity.setName(name.trim());
                entity.setLightEntitiesKeys(new ArrayList<>());
                for (String p : parts) {
                    if (dataService.contains(p.trim())) {
                        entity.getLightEntitiesKeys().add(p.trim());
                    } else {
                        Toast.info(p + " " + langMapper.response(LangMapper.GROUP_LIGHT_LABEL_NOT_FOUND), event.getChannel());
                    }
                }
                boolean s = dataService.save(entity);
                if (s) {
                    Toast.success(entity.getName() + " " + langMapper.response(LangMapper.GROUP_NEW_SUCCESS), event.getChannel());
                } else {
                    Toast.fail(entity.getName() + " " + langMapper.response(LangMapper.GROUP_NEW_EXIST), event.getChannel());
                }
            } else {
                Toast.info(langMapper.response(LangMapper.GROUP_NEW_INCORRECT_DATA), event.getChannel());
            }
        } else {
            Toast.info(langMapper.response(LangMapper.GROUP_NEW_NO_DATA), event.getChannel());
        }
    }
}
