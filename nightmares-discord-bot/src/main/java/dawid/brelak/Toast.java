package dawid.brelak;

import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.message.embed.EmbedBuilder;

import java.awt.*;

class Toast {
    static void wait(String text, TextChannel channel) {
        new MessageBuilder().setEmbed(new EmbedBuilder().setTitle(text).setColor(Color.BLUE)).send(channel);
    }
    static void on(String text, TextChannel channel) {
        new MessageBuilder().setEmbed(new EmbedBuilder().setTitle(text).setColor(Color.YELLOW)).send(channel);
    }
    static void off(String text, TextChannel channel) {
        new MessageBuilder().setEmbed(new EmbedBuilder().setTitle(text).setColor(Color.GRAY)).send(channel);
    }
    static void offline(String text, TextChannel channel) {
        new MessageBuilder().setEmbed(new EmbedBuilder().setTitle(text).setColor(Color.BLACK)).send(channel);
    }
    static void info(String text, TextChannel channel) {
        new MessageBuilder().setEmbed(new EmbedBuilder().setTitle(text).setColor(Color.WHITE)).send(channel);
    }
    static void success(String text, TextChannel channel) {
        new MessageBuilder().setEmbed(new EmbedBuilder().setTitle(text).setColor(Color.GREEN)).send(channel);
    }
    static void fail(String text, TextChannel channel) {
        new MessageBuilder().setEmbed(new EmbedBuilder().setTitle(text).setColor(Color.RED)).send(channel);
    }
}
