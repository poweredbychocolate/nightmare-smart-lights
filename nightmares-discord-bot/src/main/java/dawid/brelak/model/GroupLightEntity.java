package dawid.brelak.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class GroupLightEntity implements Serializable {
    private String name;
    private List<String> lightEntitiesKeys;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getLightEntitiesKeys() {
        return lightEntitiesKeys;
    }

    public void setLightEntitiesKeys(List<String> lightEntitiesKeys) {
        this.lightEntitiesKeys = lightEntitiesKeys;
    }

    @Override
    public String toString() {
        return "GroupLightEntity{" +
                "name='" + name + '\'' +
                ", lightEntitiesKeys=" + lightEntitiesKeys +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupLightEntity that = (GroupLightEntity) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(lightEntitiesKeys, that.lightEntitiesKeys);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lightEntitiesKeys);
    }
}
