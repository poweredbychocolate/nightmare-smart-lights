package dawid.brelak.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * The type Light Entity.
 *
 * @author Dawid
 * @version 4
 * @since 2019-08-10
 */
public class LightEntity implements Serializable {
    private String name;
    private String address;
    private String lightID;
    private Long usageCount;

    public LightEntity() {
        usageCount = 0L;
    }

     /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets usage count.
     *
     * @return the usage count
     */
    public Long getUsageCount() {
        return usageCount;
    }

    /**
     * Sets usage count.
     *
     * @param usageCount the usage count
     */
    public void setUsageCount(Long usageCount) {
        if (usageCount < (Long.MAX_VALUE - 2))
            this.usageCount = usageCount;
    }

    public String getLightID() {
        return lightID;
    }

    public void setLightID(String lightID) {
        this.lightID = lightID;
    }

    @Override
    public String toString() {
        return "LightEntity{" +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", lightID='" + lightID + '\'' +
                ", usageCount=" + usageCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LightEntity that = (LightEntity) o;
        return  Objects.equals(name, that.name) &&
                Objects.equals(address, that.address) &&
                Objects.equals(lightID, that.lightID) &&
                Objects.equals(usageCount, that.usageCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, lightID, usageCount);
    }
}
